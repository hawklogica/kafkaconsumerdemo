package com.hawk.kafkagvmt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int age;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
    private String address;
    private String maritalStatus;

    @OneToOne(cascade = CascadeType.ALL)
    private PensionProvider pensionProvider;

    public void setDateOfBirth(String date) {
        this.dateOfBirth = LocalDate.parse(date);
    }

    public void setDateOfBirth(LocalDate date) {
        this.dateOfBirth = date;
    }

}
