package com.hawk.kafkagvmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkagvmtApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkagvmtApplication.class, args);
    }

}
