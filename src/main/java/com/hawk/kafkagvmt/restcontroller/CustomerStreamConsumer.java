package com.hawk.kafkagvmt.restcontroller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hawk.kafkagvmt.model.Customer;
import com.hawk.kafkagvmt.service.CustomerService;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

@EnableKafka
@Configuration
public class CustomerStreamConsumer {

    private final CustomerService customerService;

    public CustomerStreamConsumer(CustomerService customerService) {
        this.customerService = customerService;
    }

    @KafkaListener(topics = "NewCustomerTopic", groupId = "foo")
    public void listenForNewCustomer(String message) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Customer customer = objectMapper.readValue(message, Customer.class);
            customerService.createCustomer(customer);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
