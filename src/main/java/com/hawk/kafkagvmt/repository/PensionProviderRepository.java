package com.hawk.kafkagvmt.repository;

import com.hawk.kafkagvmt.model.PensionProvider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PensionProviderRepository extends JpaRepository<PensionProvider, Integer> {
}
