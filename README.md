# Kafka consumer project

This is a Spring Boot application demoing the capabilities of Apache Kafka

## Requirements

1. Java 8+
2. Zookeeper and kafka on port 9092
3. Kafka Producer project running on any port except 8081

## Usage

1. Start the Spring-Boot project
2. By calling `localhost:8081/customers` you can see the current customers stored in the db. 
If the other project is creating customers it will be reflected here.

## Future Improvements

- Find a way to not write both model classes in both projects.
- Implement update and delete.
