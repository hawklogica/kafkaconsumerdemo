package com.hawk.kafkagvmt.service;

import com.hawk.kafkagvmt.model.Customer;
import com.hawk.kafkagvmt.model.PensionProvider;
import com.hawk.kafkagvmt.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.List;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> fetchAllCustomer() {
        return customerRepository.findAll();
    }

    public void createCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    public void updateCustomer(Customer customer) {

    }

    @PostConstruct
    private void createTestData() {
        PensionProvider pensionProvider = new PensionProvider(0, "Worst Insurance Corp");
        Customer customer = new Customer(0, "Jane Doe", 1, LocalDate.of(1991, 10, 30), "123 Street", "Married", pensionProvider);

        createCustomer(customer);
        System.out.println("-End of creating test data-");
    }

}
